<?php

namespace XLabs\ResultCacheBundle\Annotations;

use Doctrine\Common\Annotations\Annotation;

/**
 * @Annotation
 * @Target("CLASS")
 */
final class Clear extends Annotation
{
    public static $annotationName = 'XLabs\\ResultCacheBundle\\Annotations\\Clear';

    /**
     * @var array<string>
     */
    public $onFlush;

    /**
     * One or more keys.
     *
     * @var array<\XLabs\ResultCacheBundle\Annotations\Key>
     */
    public $value;
}