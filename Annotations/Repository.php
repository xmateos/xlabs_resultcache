<?php

namespace XLabs\ResultCacheBundle\Annotations;

use Doctrine\Common\Annotations\Annotation;

/**
 * @Annotation
 * @Target("CLASS")
 */
final class Repository extends Annotation
{
    public static $annotationName = 'Xlabs\\ResultCacheBundle\\Annotations\\Repository';
}