<?php

namespace XLabs\ResultCacheBundle\Annotations;

use Doctrine\Common\Annotations\Annotation;

/**
 * @Annotation
 * @Target("ANNOTATION")
 */
final class Key extends Annotation
{
    /**
     * @var array<string>
     */
    public $onFlush;

    /**
     * @var string
     *
     * @Enum({"prefix", "suffix", "regexp", "literal"})
     */
    public $type;

    /**
     * @var array<\XLabs\ResultCacheBundle\Annotations\Key>
     */
    public $method;

    /**
     * @var array<string>
     */
    public $arguments;

    /**
     * @var string
     */
    public $value;
}