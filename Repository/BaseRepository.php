<?php

namespace XLabs\ResultCacheBundle\Repository;

use Doctrine\ORM\EntityRepository;

/*
 * This method seems to be faster than ResultCache and HydrationCache; might be better in newer versions of Symfony/Doctrines
 */
class BaseRepository extends EntityRepository
{
    public function getItemById($id)
    {
        $em = $this->getEntityManager();
        $cache = $em->getConfiguration()->getResultCacheImpl();
        $cache_config = method_exists($cache, 'getConfig') ? $cache->getConfig() : false;
        if($cache_config && isset($cache_config['custom_cache_enabled']) && $cache_config['custom_cache_enabled'])
        {
            $cacheKey = $this->getCacheKey($id);
            if($cache->contains($cacheKey))
            {
                return unserialize($cache->fetch($cacheKey));
            }
            $item = $this->_getItemById($id);
            if($item)
            {
                $cache->save($cacheKey, serialize($item), constant($this->getEntityName().'::RESULT_CACHE_ITEM_TTL'));
            }
        } else {
            $item = $this->_getItemById($id);
        }
        return $item;
    }

    protected function getCacheKey($id)
    {
        $locale = '';
        if(method_exists($this, 'setLocale'))
        {
            $locale = $this->locale ? $this->locale : '';
        }
        return (method_exists($this, 'setLocale') ? ($locale.'_') : '').constant($this->getEntityName().'::RESULT_CACHE_ITEM_PREFIX').$id;
    }
}
