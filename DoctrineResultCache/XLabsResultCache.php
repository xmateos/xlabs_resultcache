<?php

namespace XLabs\ResultCacheBundle\DoctrineResultCache;

use Doctrine\Common\Cache\PredisCache;
use Predis\Client as Predis;

class XLabsResultCache extends PredisCache
{
    private $config;
    private $redis;
    private $resultCache_prefix;

    public function __construct($config)
    {
        $this->config = $config;
        $this->resultCache_prefix = isset($config['_key_namespace']) ? $config['_key_namespace'].':' : 'xlabsResultCache_';
        $this->redis = new Predis(array(
            'scheme' => 'tcp',
            'host'   => $config['redis_settings']['host'],
            'port'   => $config['redis_settings']['port'],
            'database' => $config['redis_settings']['database_id']
        ), array(
            'prefix' => $this->resultCache_prefix
        ));
        parent::__construct($this->redis);
    }

    public function getRedis()
    {
        return $this->redis;
    }

    public function getConfig()
    {
        return $this->config;
    }

    /**
     * Deletes all cache entries matching the given regular expression.
     *
     * @param string $regex
     * @return array An array of deleted cache ids
     */
    public function deleteByRegex($regex)
    {
        // Example:
        //  $em->getConfiguration()->getResultCacheImpl()->deleteByRegex('/user_79_galleries_.*/');
        $deleted = array();
        $ids = $this->getIds();
        if($ids)
        {
            $matching_ids = array();
            foreach($ids as $id)
            {
                if(preg_match($regex, $id)) {
                    $matching_ids[] = $id;
                }
            }
            if($matching_ids)
            {
                foreach($matching_ids as $id)
                {
                    $_id = substr($id, strlen($this->resultCache_prefix));
                    if($this->doDelete($_id))
                    {
                        $deleted[] = $id;
                    }
                }
            }
        }
        return $deleted;
    }

    /**
     * Deletes all cache entries beginning with the given string.
     *
     * @param string $prefix
     * @return array An array of deleted cache ids
     */
    public function deleteByPrefix($prefix)
    {
        $deleted = array();
        $ids = $this->getIds($prefix);
        if($ids)
        {
            foreach($ids as $id)
            {
                $_id = substr($id, strlen($this->resultCache_prefix));
                if($this->doDelete($_id))
                {
                    $deleted[] = $id;
                }
            }
        }
        return $deleted;
    }

    /**
     * Deletes all cache entries ending with the given string.
     *
     * @param string $suffix
     * @return array An array of deleted cache ids
     */
    public function deleteBySuffix($suffix)
    {
        $deleted = array();
        $ids = $this->getIds(null, $suffix);
        if($ids)
        {
            foreach($ids as $id)
            {
                $_id = str_replace($this->resultCache_prefix, '', $id);
                if($this->doDelete($_id))
                {
                    $deleted[] = $id;
                }
                /*$pattern = '/(?<=\[)([^\]]*' . preg_quote($suffix, '/') . ')(?=\])/';
                if(preg_match($pattern, $id, $matches))
                {
                    echo $matches[1]; // Outputs: en_mm_dvd_379
                } else {
                    echo 'No match found';
                }
                dump($id, $suffix, substr($id, -1 * strlen($suffix))); die;*/
                /*if($suffix === substr($id, -1 * strlen($suffix)))
                {
                    $_id = substr($id, strlen($this->resultCache_prefix));
                    if($this->doDelete($_id))
                    {
                        $deleted[] = $id;
                    }
                }*/
            }
        }
        return $deleted;
    }

    /**
     * Returns an array of cache ids.
     *
     * @param string $prefix
     *        	Optional id prefix
     * @return array An array of cache ids
     */
    public function getIds($prefix = null, $suffix = null)
    {
        if($prefix)
        {
            return $this->getRedis()
                ->keys($this->getNamespace() . '\[' . $prefix . '*');
        } elseif($suffix) {
            return $this->getRedis()
                ->keys($this->getNamespace() . '\[*'.$suffix.'\]*');
        } else {
            return $this->getRedis()
                ->keys($this->getNamespace() . '*');
        }
    }
}