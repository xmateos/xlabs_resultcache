<?php

namespace XLabs\ResultCacheBundle\DoctrineResultCache;

use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Component\Form\Exception\InvalidArgumentException;
use XLabs\ResultCacheBundle\Annotations\Repository as XLabsResultCacheRepository;
use XLabs\ResultCacheBundle\Annotations\Clear as XLabsResultCacheClear;
use XLabs\ResultCacheBundle\Annotations\Key as XLabsResultCacheKey;

class CacheInvalidator
{
    public function onFlush(OnFlushEventArgs $event)
    {
        $em = $event->getEntityManager();
        $uow = $em->getUnitOfWork();

        $entityChanges = array(
            'insert' => $uow->getScheduledEntityInsertions(),
            'update' => $uow->getScheduledEntityUpdates(),
            'delete' => $uow->getScheduledEntityDeletions()
        );

        $reader = new AnnotationReader();
        foreach($entityChanges as $change => $entities)
        {
            foreach($entities as $entity)
            {
                $entityClass = $em->getMetadataFactory()->getMetadataFor(get_class($entity))->getName();

                /**
                 * In case there´s inheritance, also check the parent class
                 */
                $entityClass_parent = get_parent_class($entityClass);

                $aEntities = $entityClass_parent ? array($entityClass, $entityClass_parent) : array($entityClass);

                foreach($aEntities as $entityClass)
                {
                    $ref = new \ReflectionClass($entityClass);
                    $XLabsResultCacheRepositoryAnnotation = $reader->getClassAnnotation($ref, XLabsResultCacheRepository::$annotationName);
                    if($XLabsResultCacheRepositoryAnnotation)
                    {
                        // Get repository from Doctrine annotation
                        $EntityRepositoryAnnotation = $reader->getClassAnnotation($ref, 'Doctrine\\ORM\\Mapping\\Entity');
                        if($EntityRepositoryAnnotation)
                        {
                            // On development; in case I want to move functionality in the entity specific repository
                            $entityRepository = $EntityRepositoryAnnotation->repositoryClass;
                        }
                    }
                    //$XLabsResultCacheAnnotation = $reader->getClassAnnotation($ref, 'BF\\BikiniBundle\\Annotations\\Clear');
                    $XLabsResultCacheAnnotation = $reader->getClassAnnotation($ref, XLabsResultCacheClear::$annotationName);
                    if($XLabsResultCacheAnnotation)
                    {
                        //dump($XLabsResultCacheAnnotation);
                        // insert/update/delete matches the onFlush array
                        if($XLabsResultCacheAnnotation->onFlush)
                        {
                            // Generic onFlush events for all keys
                            if(in_array($change, $XLabsResultCacheAnnotation->onFlush))
                            {
                                foreach($XLabsResultCacheAnnotation->value as $key)
                                {
                                    $onFlushMatch = true;
                                    if($key->onFlush)
                                    {
                                        if(!in_array($change, $key->onFlush))
                                        {
                                            $onFlushMatch = false;
                                        }
                                    }
                                    if($onFlushMatch && $key instanceof XLabsResultCacheKey)
                                    {
                                        $method = $this->getResultCacheClearMethod($key);
                                        if($method)
                                        {
                                            $aKeyStr = $this->getResultCacheKeyToClear($key, $entity, $entityClass, $ref);
                                            $aKeyStr = is_array($aKeyStr) ? $aKeyStr : array($aKeyStr);
                                            if(!empty($aKeyStr))
                                            {
                                                foreach($aKeyStr as $keyStr)
                                                {
                                                    $em->getConfiguration()->getResultCacheImpl()->$method($keyStr);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        } else {
                            // Key specific onFlush event
                            foreach($XLabsResultCacheAnnotation->value as $key)
                            {
                                if($key->onFlush)
                                {
                                    if(in_array($change, $key->onFlush))
                                    {
                                        if($key instanceof XLabsResultCacheKey)
                                        {
                                            $method = $this->getResultCacheClearMethod($key);
                                            if($method)
                                            {
                                                $aKeyStr = $this->getResultCacheKeyToClear($key, $entity, $entityClass, $ref);
                                                $aKeyStr = is_array($aKeyStr) ? $aKeyStr : array($aKeyStr);
                                                if(!empty($aKeyStr))
                                                {
                                                    foreach($aKeyStr as $keyStr)
                                                    {
                                                        $em->getConfiguration()->getResultCacheImpl()->$method($keyStr);
                                                    }
                                                }
                                                //mail('xavi.mateos@manicamedia.com', 'Keys', implode(', ', $aKeyStr));
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    /*if(!$XLabsResultCacheAnnotation) {
                        throw new \Exception(sprintf('Entity class %s does not have required annotation XLabsResultCache', $entityClass));
                    }*/
                }
            }
        }
    }

    private function getResultCacheClearMethod(XLabsResultCacheKey $key)
    {
        switch(strtolower($key->type))
        {
            case 'prefix':
                return 'deleteByPrefix';
                break;
            case 'suffix':
                return 'deleteBySuffix';
                break;
            case 'regex':
                return 'deleteByRegex';
                break;
            case 'literal':
                return 'delete';
                break;
            default:
                return false;
                break;
        }
    }

    private function getResultCacheKeyToClear(XLabsResultCacheKey $key, $entity, $entityClass, $ref)
    {
        $keyStr = false;
        // Static value
        if($key->value)
        {
            $keyStr = $key->value;
        } else {
            // Method
            if($key->method)
            {
                $km = $key->method;
                if(method_exists($entity, $km))
                {
                    if($key->arguments)
                    {
                        // This still requires work!!
                        $keyStr = $entity->$km($key->arguments);
                    } else {
                        $keyStr = $entity->$km();
                    }
                } else {
                    throw new \Exception(sprintf('Entity class %s does not have method %s', $entityClass, $km));
                }
            } else {
                // Generate standard ID based key
                $keyStr = strtolower($ref->getShortName()).'_'.$entity->getId();
            }
        }
        return $keyStr;
    }
}