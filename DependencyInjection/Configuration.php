<?php

namespace XLabs\ResultCacheBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('x_labs_result_cache');

        // Here you should define the parameters that are allowed to
        // configure your bundle. See the documentation linked above for
        // more information on that topic.
        $rootNode
            ->children()
                ->arrayNode('redis_settings')->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('host')->defaultValue('127.0.0.1')->end()
                        ->integerNode('port')->defaultValue(6379)->end()
                        ->integerNode('database_id')->defaultValue(6)->end()
                    ->end()
                ->end()
                ->scalarNode('_key_namespace')->defaultValue('xlabs:resultcache')->end()
                ->booleanNode('custom_cache_enabled')->defaultFalse()->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
