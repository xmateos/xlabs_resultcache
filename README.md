A redis driven result cache driver.

## Installation ##

Install through composer:

```bash
php -d memory_limit=-1 composer.phar require xlabs/resultcachebundle
```

In your AppKernel

```php
public function registerbundles()
{
    return [
    	...
    	...
    	new XLabs\ResultCacheBundle\XLabsResultCacheBundle(),
    ];
}
```

## Configuration sample
Default values are shown below:
``` yml
# app/config/config.yml
  
x_labs_result_cache:
    redis_settings:
        host: 192.168.5.23
        port: 6379
        database_id: 9
    _key_namespace: 'xlabs:resultcache'
    custom_cache_enabled: (default false)
```
  
Also, make sure you ORM definition uses the following parameters for the result_cache driver:  
``` yml
# app/config/config.yml
...
doctrine:
    ...
    orm:
        ...
        result_cache_driver:
            type: service
            id: xlabs_result_cache
```


### Create entity annotations ###
Whenever the entity manager performs a flush operation on an entity holding this annotations, it will clear the ResultCache key specified.

This is how you would set annotations for a given entity:
``` php
<?php

    use XLabs\ResultCacheBundle\Annotations as XLabsResultCache;
    ...
  
    /**
     * @ORM\Entity
     * @ORM\Table(name="myTable")
     * @XLabsResultCache\Clear(onFlush={}, {
     *      @XLabsResultCache\Key(onFlush={"update", "delete"}, type="literal", method="getXLabsResultCacheKeyForItem"),
     *      @XLabsResultCache\Key(onFlush={"insert", "delete"}, type="literal", value="bf_galleries_"),
     *      @XLabsResultCache\Key(onFlush={"insert", "delete"}, type="prefix", method="getXLabsResultCacheKeyForUserGalleries")
     * })
     */
    class myEntity
    {
        ...
    }
```
  
onFlush  
Values: "insert", "update" and "delete"  
Description: Clear cache key when the flush action matches the action listed in this variable. If set under the "Clear" annotation, it will affect all the keys listed below, unless the key has a specific "onFlush" variable set.  
  
type  
Values: "literal" | "prefix" | "suffix" | "regex"  
Description: the result cache key/s will be removed with the corresponding methods "delete", "deleteByRegex", "deleteByPrefix" or "deleteBySuffix".  
  
value  
Value: string matching the result cache key that we want to clear.
  
method  
Value: allows creating a method in the entity that will return the result cache key that we want to clear.
  
If no "value" nor "method" are set, the resulting result cache clear that would expect to clear would be, following example above, "myentity_<ENTITY_ID_HERE>", useful for ID specific queries.

If 'custom_cache_enabled' is set to 'true', then Doctrine´s Result Cache is not used, and replaced by a custom cache instead, where the whole query result is serialized and stored, improving performance.